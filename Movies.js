import React, { Component } from 'react';
import Movie from './Movie';
import axios from "axios";

class Movies extends Component {

    constructor() {
        super();

        this.state = {
            movies: []
        }
    }

    componentDidMount() {
        axios.get("https://api.themoviedb.org/3/movie/popular?api_key=99c26306be3d1ce05090f2450ff90c6e&language=en-US&page=1").then((dataObj) => {
            debugger;
            console.log(dataObj);
            this.setState({ movies: dataObj.data.results });
        })
        // setTimeout(() => {
        //     this.setState({
        //         movies: [{
        //             id: 1,
        //             name: "Dark Knight",
        //             img: "https://image.tmdb.org/t/p/w185_and_h278_bestv2/1hRoyzDtpgMU7Dz4JF22RANzQO7.jpg"
        //         }, {
        //             id: 2,
        //             name: "Truman Show",
        //             img: "https://image.tmdb.org/t/p/w185_and_h278_bestv2/EelZzudHRvJmjWccWscN1S5vmI.jpg"
        //         }, {
        //             id: 3,
        //             name: "Wreck It Ralph",
        //             img: "https://image.tmdb.org/t/p/w185_and_h278_bestv2/93FsllrXXWncp7BQYTdOU1XMRXo.jpg"
        //         }, {
        //             id: 4,
        //             name: "Rangasthalam",
        //             img: "https://image.tmdb.org/t/p/w185_and_h278_bestv2/yiEzDgBBFC25Zd6z0r7sMngn5vr.jpg"
        //         }, {
        //             id: 5,
        //             name: "Baahubali",
        //             img: "https://image.tmdb.org/t/p/w185_and_h278_bestv2/sXf30F2HFpsFPXlNz7jpOySSV9I.jpg"
        //         }]
        //     })
        // }, 2000);
    }

    render() {
        return <div>
            <h2>Movies List</h2>
            {
                this.state.movies.length > 0 ?
                    this.state.movies.map((e) => {
                        return <Movie key={e.id} obj={e} />
                    })
                    : <h3>Loading ...</h3>
            }
        </div>
    }
}

export default Movies;
